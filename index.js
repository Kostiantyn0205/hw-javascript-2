/*1) типи данних: number, string, boolean, null, undefined, object*/
/*2) Оператор == неспроможна відрізнити 1 від "1" так як воно автоматично приводить типи до одного, а оператор === строге порівняння без приведення типів воно спочатку порівнює типи а потім якщо типи однакові порівнює значення*/
/*3) Оператор це символ який дозволяє робити якісь дії над змінними, вони бувають декількох типів: арифметичні, логічні, присвоєння, порівняння, умовні*/

let userName = prompt("enter your name");
while (userName === "" ) {
    alert("Please enter valid information!");
    userName = prompt("Enter your name:", userName);
}

let userAge = prompt("enter your age");
while (isNaN(userAge) || userAge === "") {
    alert("Please enter valid information!");
    userAge = prompt("Enter your age:", userAge);
}

if(userAge<18){
    alert("You are not allowed to visit this website");
}else if (userAge>=18 && userAge<=22){
    const agreement = confirm("Are you sure you want to continue?");
    if(agreement){
        alert("Welcome "+userName);
    }else{
        alert("You are not allowed to visit this website");
    }
}else{
    alert("Welcome "+userName);
}
